using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("IceBullet") || collision.gameObject.CompareTag("BoneHell") || collision.gameObject.CompareTag("Flame"))
        {
            Debug.Log(collision.gameObject.tag);
            Destroy(collision.gameObject);
        }
    }
}
