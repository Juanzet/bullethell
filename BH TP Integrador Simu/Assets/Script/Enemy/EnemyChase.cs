using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChase : MonoBehaviour
{
    public Transform target;
    public float speed = 4f;
    float damageCd = 1.5f;
    float nextDamage = 0.0f;

    PlayerController hp;
    public float myDamage;

    Rigidbody2D rb2D;

    [SerializeField] LevelManager deadPoints;
    [SerializeField] int pointsPerDead;
     
    void Start()
    {
        //GameObject.FindGameObjectWithTag("Player");
        rb2D = GetComponent<Rigidbody2D>();
    }

    
    void FixedUpdate()
    {
        EnemyChaseLogic();
    }

    void EnemyChaseLogic()
    {
        Vector3 position = Vector3.MoveTowards(transform.position, target.position, speed * Time.fixedDeltaTime);
        rb2D.MovePosition(position);
        //transform.LookAt(target);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Flame")
        {
            if(Time.time > nextDamage)
            {
                nextDamage = Time.time + damageCd;
                deadPoints.AddPoints(pointsPerDead);
                Destroy(gameObject);
            }   
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            hp = collision.gameObject.GetComponent<PlayerController>();
            hp.TakeDamage(myDamage);
        }
    }
   
}
