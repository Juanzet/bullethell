using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject easyEnemy;
    [SerializeField]
    private GameObject hardEnemy;

    [SerializeField]
    private float easyEnemyInterval = 3.5f;
    [SerializeField]
    private float hardEnemyInterval = 10f;

    void Start()
    {
        StartCoroutine(spawnEnemy(easyEnemyInterval, easyEnemy));
        StartCoroutine(spawnEnemy(hardEnemyInterval, hardEnemy));
    }

    private IEnumerator spawnEnemy(float interval, GameObject enemy)
    {
        yield return new WaitForSeconds(interval);
        GameObject newEnemy = Instantiate(enemy, new Vector3(Random.Range(-5f, 5), Random.Range(-6f, 6f), 0), Quaternion.identity);
        StartCoroutine(spawnEnemy(interval, enemy));
    }


}
