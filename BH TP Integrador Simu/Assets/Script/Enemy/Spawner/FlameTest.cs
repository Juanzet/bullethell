using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameTest : MonoBehaviour
{
    [SerializeField] float spawnTime = 0.5f;
    [SerializeField] GameObject prefab;
    
    void Start()
    {
        StartCoroutine(FlameInstanciate());
    }

    IEnumerator FlameInstanciate()
    {  
        GameObject go = Instantiate(prefab, this.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(spawnTime);

        Destroy(go, 1f);
    }

}
