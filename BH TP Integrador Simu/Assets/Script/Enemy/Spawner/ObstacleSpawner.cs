using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField] GameObject[] obstaclePrefab;
    [SerializeField] float secSpawn = 0.5f;
    [SerializeField] float minTransform;
    [SerializeField] float maxTransform;

    void Start()
    {
        StartCoroutine(ObstacleSpawn());
    }

    IEnumerator ObstacleSpawn()
    {
        while (true)
        {
            var wanted = Random.Range(minTransform, maxTransform);
            var position = new Vector3(wanted, transform.position.y);
            GameObject go = Instantiate(obstaclePrefab[Random.Range(0, obstaclePrefab.Length)], position, Quaternion.identity);
            yield return new WaitForSeconds(secSpawn);
            Destroy(go, 5f);
        }
    }
 
}
