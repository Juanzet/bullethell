using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeathBulletPatern : MonoBehaviour
{
    #region Set patern
    public float phase1;
    public float phase2;
    public float phase3;

    public GameObject bulletPatern1;
    public GameObject bulletPatern2;
    public GameObject bulletPatern3;
    #endregion
    HealtBar hp = new HealtBar();
    void Start()
    {
        bulletPatern1.SetActive(false);
        bulletPatern2.SetActive(false);
        bulletPatern3.SetActive(false);
    }

    void Update()
    {
        EnemyPhase();
    }

    public void EnemyPhase()
    {
        if (hp.healthAmount < phase1)
        {
            bulletPatern1.SetActive(true);
        }
        else if (hp.healthAmount < phase2 && phase1 > hp.healthAmount)
        {
            bulletPatern1.SetActive(false);
            bulletPatern2.SetActive(true);
        }
        else if (hp.healthAmount < phase3 && phase1 > hp.healthAmount && phase2 > hp.healthAmount)
        {
            bulletPatern1.SetActive(false);
            bulletPatern2.SetActive(false);
            bulletPatern3.SetActive(true);
        }
    }
}
