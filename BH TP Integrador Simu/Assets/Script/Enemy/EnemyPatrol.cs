using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public Transform[] patrolPoints;
    public int targetPoint;
    public float speed;
    public float spinSpeed;
    float time;

    void Start()
    {

        targetPoint = 0;
    }
    void FixedUpdate()
    {
        time += Time.deltaTime;

        transform.rotation = Quaternion.Euler(0, 0, time * spinSpeed);
    }


    void Update()
    { 
        // si  la posicion del enemigo es igual a la posicion del patron sumar una posicion
        if (transform.position == patrolPoints[targetPoint].position)
        {
            // pasa un valor random del array 
           targetPoint = Random.Range(0, patrolPoints.Length);
            //IncreaseOrDecreaseTargetInt();
        }

        // mover nuestro enemigo entre cada posicion sumando el objetivo de a 1
        transform.position = Vector3.MoveTowards(transform.position, patrolPoints[targetPoint].position, speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Flame"))
        {
            // soltar armar + destruirse + abrir portal
        }
    }

    /* Este metodo no tiene sentido porque en vez de reecorrer los puntos 1x1 los recorre dee manera aleatoria 
    void IncreaseOrDecreaseTargetInt()
    {
        targetPoint++;
        // si los puntos del objetivo son mayores que el array hacer que sea 0 
        if (targetPoint >= patrolPoints.Length)
        {
            targetPoint = 0;
        }
    }
    */
}
