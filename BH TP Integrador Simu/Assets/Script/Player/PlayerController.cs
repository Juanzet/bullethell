using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rb;
    SpriteRenderer _thisPlayer;
    Animator anim;
    #region Movement
    public ParticleSystem particleDash;
    public float walkSpeed = 4f;
    public float speedLimiter = 0.7f;
    float inputHorizontal;
    float inputVertical;
    #endregion
    #region dash
    bool canDash = true;
    bool isDashing;
    public float dashingSpeed;
    public float dashingTime = 0.3f;
    public float dashingCD = 1f;
    //public CapsuleCollider2D collider;
    #endregion
    #region health
    public TMP_Text lifeText;
    public Image healthBarUI;
    public float healthAmount;
    public float healtDivide;
    float damageTake;
    #endregion

    void Awake()
    {
        anim = GetComponent<Animator>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        //collider = GetComponent<CapsuleCollider2D>();
        _thisPlayer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        anim.SetBool("Idle", true);
        lifeText.text = $"HP: {healthAmount}";
    }

    void Update()
    {
        #region Cheats
        if (Input.GetKey(KeyCode.P))
        {
            healtDivide += 100;
            healthAmount += 100;
        }

        if (Input.GetKey(KeyCode.O))
        {
            healtDivide -= 100;
            healthAmount -= 100;
        }

        if (Input.GetKey(KeyCode.L))
            SaveManager.instance.SavePortalData();

        if (Input.GetKey(KeyCode.K))
            SaveManager.instance.LoadPortalData();
        #endregion
        #region move & dash
        if (isDashing)
            return;

        PlayerMovement();
        
        if(Input.GetKeyDown(KeyCode.LeftShift) && canDash)
        {
            particleDash.gameObject.SetActive(true);
            this.gameObject.tag = "Untagged";
            //collider.enabled = false;
            _thisPlayer.enabled = false;
            StartCoroutine(Dashing());
        }
        #endregion

        if (healthAmount <= 0)
        {
            anim.SetBool("Dead", true);
            StartCoroutine(Dead());
        }
    }

    void FixedUpdate()
    {
        if (isDashing)
            return;

        PlayerPhysicMov();
    }
    #region movement & dash logic
    void PlayerMovement()
    {
        if (DialogueManager.isActive)
            return;

        inputHorizontal = Input.GetAxisRaw("Horizontal");
        inputVertical = Input.GetAxisRaw("Vertical");

        if (inputHorizontal < 0)
            _thisPlayer.flipX = true;
        else if (inputHorizontal > 0)
            _thisPlayer.flipX = false;

        anim.SetFloat("speedX", inputHorizontal);
        anim.SetFloat("speedY", inputVertical);
    }

    void PlayerPhysicMov()
    {
        
        if (inputHorizontal != 0 || inputVertical != 0)
        {
            if (inputHorizontal != 0 && inputVertical != 0)
            {
                inputHorizontal *= speedLimiter;
                inputHorizontal *= speedLimiter;
            }

            rb.velocity = new Vector2(inputHorizontal * walkSpeed, inputVertical * walkSpeed);
        }
        else
        {
            rb.velocity = new Vector2(0f, 0f);
        }
    }

    private IEnumerator Dashing()
    {
        var x = Input.GetAxisRaw("Horizontal");
        var y = Input.GetAxisRaw("Vertical");

        canDash = false;
        isDashing = true;
        rb.velocity = new Vector2(x * dashingSpeed, y * dashingSpeed);
        yield return new WaitForSeconds(dashingTime);
        particleDash.gameObject.SetActive(false);
        this.gameObject.tag = "Player";
        //collider.enabled = true;
        _thisPlayer.enabled = true;
        isDashing = false;
        yield return new WaitForSeconds(dashingCD); 
        canDash = true;
    }
    #endregion
    private IEnumerator Dead()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Lobby");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("IceBullet") || collision.gameObject.CompareTag("BoneHell"))
        {
            Destroy(collision.gameObject);
            Debug.Log("El objeto " + collision.gameObject.name + " hizo contacto con el jugador");
            // logica de reduccion de vida
        }
    }
   
    #region healthLogic
    public void TakeDamage(float damage)
    {
        healthAmount -= damage;
        healthBarUI.fillAmount = healthAmount / healtDivide;
        lifeText.text = $"HP: {healthAmount}";
    }

    public void Healing(float healthPoints)
    {
        healthAmount += healthPoints;
        healthAmount = Mathf.Clamp(healthAmount, 0, healtDivide);

        healthBarUI.fillAmount = healthAmount / healtDivide;
        lifeText.text = $"HP: {healthAmount}";
    }
    #endregion
}
