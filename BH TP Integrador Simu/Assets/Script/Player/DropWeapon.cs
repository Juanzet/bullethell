using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropWeapon : MonoBehaviour
{
    [SerializeField] private GameObject automatic;
    [SerializeField] private GameObject shotgun;

    private void Start()
    {
        automatic.SetActive(false);
        shotgun.SetActive(false);
    }
    public void DropRandomWeapon()
    {
        float random = Random.Range(0f, 1f);

        if(random >= 0)
        {
            automatic.SetActive(true);

        } else if( random <= 1)
        {
            shotgun.SetActive(true);
        }
    }
}
