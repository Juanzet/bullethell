using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    private Camera mainCamera;
    private Vector3 mousePos;
    private float timer;

    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform[] bulletTransforms;
    [SerializeField] private bool canFire;
    [SerializeField] private float timeBetweenFiring;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        HandleShooting();
        RotateTowardsMouse();
    }

    private void RotateTowardsMouse()
    {
        mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direction = mousePos - transform.position;

        // Convert direction to rotation in degrees
        float rotationZ = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0, 0, rotationZ);
    }

    private void HandleShooting()
    {
        if (!canFire)
        {
            timer += Time.deltaTime;
            if (timer > timeBetweenFiring)
            {
                canFire = true;
                timer = 0;
            }
        }

        if (Input.GetMouseButton(0) && canFire)
        {
            FireBullets();
        }
    }

    private void FireBullets()
    {
        canFire = false;
        foreach (Transform bulletTransform in bulletTransforms)
        {
            Instantiate(bullet, bulletTransform.position, Quaternion.identity);
        }
    }
}
