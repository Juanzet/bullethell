using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Vector3 mousePos;
    Camera mainCamera;
    Rigidbody2D rb;
    HealtBar hb;

    public float force;
    public float myDamage;
     
    void Awake()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        BulletDirectionAndRotation();
    }


    void Update()
    {
        Destroy(this.gameObject, 2f);
    }

    void BulletDirectionAndRotation()
    {
        mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);

        Vector3 direction = mousePos - transform.position;
        // esto es para que la bala gire en direccion del mouse
        Vector3 rotation = transform.position - mousePos;
        rb.velocity = new Vector2(direction.x, direction.y).normalized * force;
        float rot = Mathf.Atan2(rotation.y, rotation.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, rot + 90);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            hb = collision.gameObject.GetComponent<HealtBar>();
            hb.TakeDamage(myDamage);
            Destroy(gameObject, 0.3f);
        }
    }
}
