using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject door;
    private int enemyDead;
    /// <summary>
    /// Este script solo funciona en la escena tutorial y su funcion es decir que portal nos deberia de abrir en caso de perder o ganar
    /// </summary>
    void Update()
    {
        if(enemyDead >= 8)
        {
            door.SetActive(true);
            BossDeathCheck.bossDefeat = 2;
        } else
        {
            door.SetActive(false);
            BossDeathCheck.bossDefeat = 1;
        }
    }

    public void AddPoints(int deadPoints)
    {
        enemyDead += deadPoints;
        Debug.Log(enemyDead);
    }
}
