using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class SaveManager  : MonoBehaviour
{
    /// <summary>
    /// TODO Necesitamos persistir no solo el entero para el singleton level sino tambien la escena
    /// </summary>
    public static SaveManager instance;
    public SingletonLevel singletonLevel;

    public int portalNumber; 
    string fileSave = "GameData.dat";

    void Awake()  // TODO esta es la persistencia hay que buscar la forma en que el jugador guarde y cargue partida
    {
        if (SaveManager.instance == null)
        {
            SaveManager.instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        Debug.Log("Se ejecuto el singleton del script llamado: SaveManager");

        //filePath = Path.Combine(Application.dataPath, "portalData.dat");
       
    }

    public void SavePortalData()
    {
        string filePath = Application.persistentDataPath + "/" + fileSave;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, singletonLevel);
        portalNumber = singletonLevel.bossNumber;
        Debug.Log("Portal guardado numero: " + portalNumber);
        file.Close();

        //BossDeathCheck reference = new BossDeathCheck();
        //portalNumber = singletonLevel.bossNumber;
        //reference.portalNumber = portalNumber; // almacenamos el numero del boss 

        // testear FileStream fileStream = File.Create(Application.persistentDataPath + "/GameData.text");

        #region primer version persistencia
        //portalNumber = singletonLevel.bossNumber;
        ////Stream stream = new FileStream(filePath, FileMode.Create);
        ////BinaryFormatter binaryFormatter = new BinaryFormatter();
        ////binaryFormatter.Serialize(stream, singletonLevel.bossNumber);
        //Debug.Log("Portal guardado numero: " + portalNumber);
        //stream.Close();
        #endregion
    }

    public void LoadPortalData()
    {
        string filePath = Application.persistentDataPath + "/" + fileSave;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            SingletonLevel cargado = (SingletonLevel)bf.Deserialize(file);
            singletonLevel = cargado;
            file.Close();
            Debug.Log("Datos Cargados numero de portal: " + cargado.bossNumber);
        }
        #region primer version persistencia
        //if (File.Exists(filePath))
        //{
        //    // testear FileStream fileStream = File.Open(Application.persistentDataPath + "/GameData.text", FileMode.Open);

        //    Stream stream = new FileStream(filePath, FileMode.Open);
        //    BinaryFormatter binaryFormatter = new BinaryFormatter();
        //    //BossDeathCheck data = (BossDeathCheck)binaryFormatter.Deserialize(stream);
        //    SingletonLevel data = (SingletonLevel)binaryFormatter.Deserialize(stream);
        //    BossDeathCheck.bossDefeat = data.bossNumber; // devolvemos el valor cargado al static que guarda el valor para abrirlo en el portal
        //                                                 // <PortalNumber> solo sirve como almacenamiento momentaneo

        //    SceneManager.LoadScene(data.bossNumber, LoadSceneMode.Single);
        //    Debug.Log(BossDeathCheck.bossDefeat + "Verificar en este lugar si se cargo un numero de boss");
        //    stream.Close();
            
        //    Debug.Log(BossDeathCheck.bossDefeat + "Cargo un numero?");
        //} 
        //else
        //{
        //    Debug.Log("No hay ningun dato en el {filePath}");
        //}
        #endregion
    }
}
