using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Image actorImage;
    public TMP_Text actorName;
    public TMP_Text messageText;
    public GameObject backgroundBox;
    

    Message[] currentMessages;
    Actor[] currentActors;
    int activeMessage = 0;
    public static bool isActive = false;
   

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isActive)
        {
            NextMessage();
        }    
    }

    public void OpenDialogue(Message[] messages, Actor[] actors) // es la coneccion entre el dialogue trigger y el dialogue manager, referencia lo escrito en el inspector
    {
        currentActors = actors;
        currentMessages = messages;
        activeMessage = 0;
        backgroundBox.SetActive(true);

        isActive = true;
        Debug.Log("Mesajes cargados, conversacion en curso " + messages.Length);
        DisplayMessages();
    }

    void DisplayMessages() // mostrar textos, nombres y sprites en pantalla
    {
        Message messageToDisplay = currentMessages[activeMessage];
        messageText.text = messageToDisplay.message;

        Actor actorToDisplay = currentActors[messageToDisplay.actorId];
        actorName.text = actorToDisplay.name;
        actorImage.sprite = actorToDisplay.sprite;
    }

    public void NextMessage()
    {
        activeMessage++;
        if(activeMessage < currentMessages.Length)
        {
            DisplayMessages();
        }
        else
        {
            Debug.Log("Conversacion terminada");
            backgroundBox.SetActive(false);
            isActive = false;
        }
    }

    public void ActiveCollider(int msg, BoxCollider2D collider)
    {
        if(msg > currentMessages.Length)
        {
            collider.enabled = true;
        } 
        else
        {
            collider.enabled = false;
        }
    }

}
