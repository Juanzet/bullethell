using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletTest : MonoBehaviour
{
    public Vector2 velocity;
    public float speed;
    public float rotation;
    public float timeToDestroy;
    void Start()
    {
        transform.rotation = Quaternion.Euler(0, 0, rotation);
    }
    void Update()
    {
        Destroy(this.gameObject, timeToDestroy);
        transform.Translate(velocity * speed * Time.deltaTime);
    }
}
