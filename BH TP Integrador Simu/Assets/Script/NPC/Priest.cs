using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Priest : MonoBehaviour
{
    bool playerInRange = false;
    public DialogueTrigger dialogueTrigger;
    public BoxCollider2D collider;
    public DialogueManager dialogueManager;

    int msg;

    void Update()
    {
        DialogueActive();
    }

    void DialogueActive()
    {  
       if (Input.GetKeyDown(KeyCode.E) && playerInRange)
       {
            msg = 0;
            collider.enabled = false;
            dialogueTrigger.StartDialogue();
       }

       if (Input.GetKeyDown(KeyCode.E))
       {
            msg++;
            dialogueManager.ActiveCollider(msg, collider);
       }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerInRange = true;
        }
    }
   
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerInRange = false;
        }
    }
}
