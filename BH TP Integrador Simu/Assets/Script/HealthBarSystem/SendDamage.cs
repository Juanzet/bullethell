using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendDamage : MonoBehaviour
{
    PlayerController hp;
    public float myDamage;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            hp = collision.gameObject.GetComponent<PlayerController>();
            hp.TakeDamage(myDamage);
            Destroy(gameObject);
        }
    }
   
}
