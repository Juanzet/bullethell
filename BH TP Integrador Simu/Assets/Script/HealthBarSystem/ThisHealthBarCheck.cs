using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThisHealthBarCheck : MonoBehaviour
{
    /// <summary>
    /// Este script esta en cada jefe del juego y su funcion es indicar que portal deberia de abrirse para si ganamos
    /// </summary>
    public int portalNumber;
    HealtBar hp = new HealtBar();
    void Update()
    {
        CheckBossLife();
    }
    
    void CheckBossLife()
    {
        if (hp.healthAmount <= 0)
        {
            BossDeathCheck.bossDefeat = portalNumber;
        }
    }
}
