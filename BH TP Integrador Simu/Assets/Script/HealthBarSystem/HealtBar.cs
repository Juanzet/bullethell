using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image healthBarUI;
    [SerializeField] private float healthAmount;
    [SerializeField] private float healthDivide;
    [SerializeField] private GameObject door;

    [SerializeField] private float phase1;
    [SerializeField] private float phase2;
    [SerializeField] private float phase3;
    [SerializeField] private GameObject p1;
    [SerializeField] private GameObject p2;
    [SerializeField] private GameObject p3;

    private void Update()
    {
        CheckHealth();
    }

    private void CheckHealth()
    {
        if (healthAmount <= 0)
        {
            ActivateDoor();
            Destroy(gameObject);
        }
        else if (healthAmount < phase1)
        {
            ActivatePhase(p1, "fase1 jeje");
        }
        else if (healthAmount < phase2)
        {
            TransitionPhase(p1, p2, 200, "fase2 jeje");
        }
        else if (healthAmount < phase3)
        {
            ActivatePhase(p3, "fase3 jeje");
        }
    }

    private void ActivateDoor()
    {
        door.SetActive(true);
    }

    private void ActivatePhase(GameObject phaseObject, string debugMessage)
    {
        phaseObject.SetActive(true);
        Debug.Log(debugMessage);
    }

    private void TransitionPhase(GameObject previousPhase, GameObject nextPhase, float healthBoost, string debugMessage)
    {
        Destroy(previousPhase);
        healthAmount += healthBoost;
        nextPhase.SetActive(true);
        Debug.Log(debugMessage);
    }

    public void TakeDamage(float damage)
    {
        healthAmount -= damage;
        UpdateHealthBar();
    }

    public void Heal(float healthPoints)
    {
        healthAmount += healthPoints;
        healthAmount = Mathf.Clamp(healthAmount, 0, healthDivide);
        UpdateHealthBar();
    }

    private void UpdateHealthBar()
    {
        healthBarUI.fillAmount = healthAmount / healthDivide;
    }
}
