using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class SingletonLevel : MonoBehaviour
{
    [Range(0, 4)]
    public int bossNumber;

    public List<GameObject> portals;
    public static SingletonLevel instance;

    private string filePath;

    private const string PortalTagPrefix = "Portal0";

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        filePath = Path.Combine(Application.dataPath, "portalData.dat");
        bossNumber = BossDeathCheck.bossDefeat;
        InitializePortals();
    }

    private void Update()
    {
        if (bossNumber == 0)
        {
            SetAllPortalsActive(false);
        }
        else
        {
            WhoIsTheBoss();
        }
    }

    private void InitializePortals()
    {
        portals = new List<GameObject>();
        for (int i = 1; i <= 4; i++)
        {
            GameObject portal = GameObject.FindGameObjectWithTag($"{PortalTagPrefix}{i}");
            if (portal != null)
            {
                portals.Add(portal);
            }
        }
    }

    private void SetAllPortalsActive(bool isActive)
    {
        foreach (var portal in portals)
        {
            portal.SetActive(isActive);
        }
    }

    public void CheckScene()
    {
        InitializePortals();
        Debug.Log("Estoy Funcionando (CheckScene)");
    }

    public void WhoIsTheBoss()
    {
        if (File.Exists(filePath))
        {
            bossNumber = SaveManager.instance.portalNumber;
        }

        SetAllPortalsActive(false);

        if (bossNumber > 0 && bossNumber <= portals.Count)
        {
            portals[bossNumber - 1].SetActive(true);
        }
    }
}
