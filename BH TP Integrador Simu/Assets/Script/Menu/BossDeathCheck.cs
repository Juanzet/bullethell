using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class BossDeathCheck : MonoBehaviour
{
    public static BossDeathCheck instance;

    public static int bossDefeat;

    public int portalNumber; // aca guardamos el numero del portal para llamarlo desde el level manager y evitar errores por singletones (modificar mas adelante)
    /// <summary>
    /// Este script es el traslador de informacion entre los jefes y el singleton level, por aca pasa la informacion entre cada jefe
    /// </summary>
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        } 
        else
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        portalNumber = bossDefeat;   
    }
}
