using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EndManager : MonoBehaviour
{
    public TMP_Text tmp;
    public float speed;
    public GameObject button;

    void Start()
    {
        button.SetActive(false);
    }
    void Update()
    {
        TextMove();
        ButtonVisible();
    }

    void TextMove()
    {
        tmp.transform.Translate(tmp.transform.up * speed * Time.deltaTime);
    }
    void ButtonVisible()
    {
        if(tmp.transform.position.y >= 600)
        {
            button.SetActive(true);
        }
    }

    public void Exit()
    {
        Application.Quit();
    }
}
