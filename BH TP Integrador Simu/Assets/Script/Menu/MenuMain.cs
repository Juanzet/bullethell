using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class MenuMain : MonoBehaviour
{
    public GameObject loadButton;
    public GameObject controlsText;
    public GameObject noData; // es txt
    
    string filePath;


    void Awake()
    {
        filePath = Path.Combine(Application.dataPath, "portalData.dat");
    }

    private void Start()
    {
        CargarData();
        controlsText.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        SceneManager.LoadScene("GP02");
    }

    
    public void InitDataBase()
    {
        SaveManager.instance.LoadPortalData();
    }
    public void ControlsCall()
    {
        controlsText.SetActive(true);
    }
    /// <summary>
    /// TODO esto ya no funciona pero hay que corregirlo, necesitamos un boton de cargar partida en el inicio
    ///  y que nos lleve a la escena lobby para poder entrar al portal que cargamos
    /// </summary>
    public void CargarData() 
    {
        if (File.Exists(filePath))
        {
            noData.SetActive(false);
            loadButton.SetActive(true);

        } 
        else
        {
            noData.SetActive(true);
            loadButton.SetActive(false);
        }
    }
}
